House cleaning: docker-compose rm -f
Remove Dangling: docker rmi -f $(docker images -qf dangling=true)

# Blueprints
- Instead of having all our routes in the app.py file
- We can break application into components 
- Easing add namespace prefix in 1 spot
- Easy to share code between modules

# Static files
- By default Flask will look for a 'static' folder to serve files
- We can override the name to something else, but its convention and default
- We put CSS like Bootstrap in vendor folders to separate them from our own styling

# Why sabang/blueprints/admin/templates/admin
- Allows us to override BP template from main template folder easier

# Jinja2 Template Macros
- These are like functions
- With context means "Any context sent to this file is avail in macro - such as a form"

#Middleware
- Sits inbetween WSGI server and flask app
- Augment details sent from WSGI before sent to Flask

# Fixing IP with Middleware
- ProxyFix is middleware from WSGI
- Users will hit nginx before they reach Flask app