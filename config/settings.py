from datetime import timedelta

DEBUG = False
SECRET_KEY = 'lalalala'
LOG_LEVEL = 'DEBUG' #CRITICAL, ERROR, WARNING, INFO


# Flask Mail
MAIL_DEFAULT_SENDER = 'fake@gmail.com' # If we dont define a sender use this
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = 'test@gmail.com'
MAIL_PASSWORD = 'Hahaha'

# Celery
CELERY_BROKER_URL = 'redis://:xXValhallaXx@redis:6379/0'
CELERY_RESULT_BACKEND = 'redis://:xXValhallaXx@redis:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_REDIS_MAX_CONNECTIONS = 5

SQLALCHEMY_DATABASE_URI = 'postgresql://sabang_db:xXValhallaXx@postgres:5432/sabang_db'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Seed User DB Details
SEED_ADMIN_EMAIL = 'dummy@data.com'
SEED_ADMIN_PASSWORD = 'defaultpw'
SEED_FIRST_NAME = 'Dummy'
SEED_LAST_NAME = 'User'
# Belongs to Flask-Login extension - changing the default cookie duration
REMEMBER_COOKIE_DURATION = timedelta(days=1)