from functools import wraps

from flask import flash, redirect
from flask_login import current_user

def anoymous_required(url='/admin/dashboard'):
    """
        Redirects user if they are logged in already
        :param url: string
        :return Function
    """
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            # current_user if from flask-login
            # is_authenticated returns if user is logged in or not
            if current_user.is_authenticated:
                return redirect(url)
            return f(*args, **kwargs)
        return decorated_function
    return decorator
        
