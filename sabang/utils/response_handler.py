from flask import jsonify

def prep_resp(data=None, msg=None, success=True):
    resp = {
        "success": success,
        "data": {
            "msg": msg,
            "data": data
        }
    }
    return jsonify(resp)