import time

from sabang.extensions import db

class ResourceMixin(object):
    # Using class inheritence we can apply these to all other Models
    # Keep track when records are created and updated.
    created_on = db.Column(db.Integer,
                           default=time.time())
    updated_on = db.Column(db.Integer,
                           default=time.time(),
                           onupdate=time.time())

    def save(self):
        db.session.add(self)
        db.session.commit()

        return self

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def __str__(self):
        # Overrides the python __str__ method and outputs model in a human readable way
        obj_id = hex(id(self))
        columns = self.__table__.c.keys()

        values = ', '.join("%s=%r" % (n, getattr(self, n)) for n in columns)
        return '<%s %s(%s)>' % (obj_id, self.__class__.__name__, values)
