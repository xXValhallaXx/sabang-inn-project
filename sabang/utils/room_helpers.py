from sabang.database.room_models import Item, Price, Room
from sabang.database.apartment_models import Apt_Item, Apt_Price, Apartment
from sabang.extensions import db

def parse_room_data(room_list):
    """
    Function to parse Rooms and Apartments
    Prepares the data for Front End
    """
    parsed_data = []
    for room in room_list:
        data = {
            'id': room.id,
            'description': room.description,
            'name': room.name,
            'info': room.info
        }
        parsed_data.append(data)
    return parsed_data


def selected_room_data(room_data):
    room_prices = room_data.prices.all()
    room_items = room_data.items.all()
    room_images = room_data.images.all()
    parsed_data = []
    price_data = []
    image_data = []
    item_data = []
    room_info = {
        'id': room_data.id,
        'description': room_data.description,
        'name': room_data.name,
        'info': room_data.info,
    }
    parsed_data.append(room_info)
    for price in room_prices:
        test = {
            "price_id": price.id,
            "price": price.price
        }
        price_data.append(test)
    room_info.update({'prices': price_data})
    for item in room_items:
        items = {
            "item_id": item.id,
            "item": item.item_name
        }
        item_data.append(items)
    room_info.update({'items': item_data})
    for image in room_images:
        current_img = {
            "image_id": image.id,
            "room_id": image.room_id,
            "avatar": image.avatar,
            "img_url": image.img_url
        }
        image_data.append(current_img)
    room_info.update({'images': image_data})
    return room_info


def delete_rooms(rooms, update_type):
    if update_type == "room":
        for room in rooms:
            result = Room.query.get(room['roomID'])
            if result is not None:
                db.session.delete(result)
                db.session.commit()
    
    if update_type == "apt":
        for room in rooms:
            result = Apartment.query.get(room['roomID'])
            if result is not None:
                db.session.delete(result)
                db.session.commit()

    return "Success"

def add_room(room_data, update_type):
    if update_type == "room":
        new_room = {
            'name': room_data['name'],
            'description': room_data['desc'],
            "info": room_data['info']
        }
        room_price = {
          'price': room_data['price'],
        }
        current_room = Room(**new_room).save()
        Price(room=current_room, **room_price).save()
    
    if update_type == "apt":
        new_apt = {
            'name': room_data['name'],
            'description': room_data['desc'],
            "info": room_data['info']
        }
        apt_price = {
          'price': room_data['price'],
        }
        current_apt = Apartment(**new_apt).save()
        Apt_Price(apartment=current_apt, **apt_price).save()

    return "Success"

def update_items(items, update_type):
    if update_type == "room":
        for item in items:
            result = Item.query.get(item['itemID'])
            if result is not None:
                if item['checked'] is True:
                    Item.query.filter(Item.id == item['itemID']).delete()
                else:
                    result.item_name = item['itemName']
            result.save()
    
    if update_type == "apt":
        for item in items:
            result = Apt_Item.query.get(item['itemID'])
            if result is not None:
                if item['checked'] is True:
                    Apt_Item.query.filter(Apt_Item.id == item['itemID']).delete()
                else:
                    result.item_name = item['itemName']
            result.save()

    return "Success"


def update_prices(price_data, update_type):
    if update_type == "room":
        for price in price_data:
            result=Price.query.get(price['roomPriceID'])
            if result is not None:
                if price['checked'] is True:
                    Price.query.filter(Price.id == price['roomPriceID']).delete()
                else:
                    # result.room_type = price['roomType']
                    # result.number_people = price['roomCapacity']
                    result.price = price['roomPrice']
            result.save()
    
    if update_type == "apt":
        for price in price_data:
            result=Apt_Price.query.get(price['roomPriceID'])
            if result is not None:
                if price['checked'] is True:
                    Apt_Price.query.filter(Apt_Price.id == price['roomPriceID']).delete()
                else:
                    # result.room_type = price['roomType']
                    # result.number_people = price['roomCapacity']
                    result.price = price['roomPrice']
            result.save()

    return "Success"