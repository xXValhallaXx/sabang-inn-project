try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin

from flask import request

def safe_next_url(target):
    """
        Ensure relative URL path is on same domain as host
        Protects from "Open redirect vulnerability"
    """
    # Prefixes path with full host URL
    # Without this app is vulnerable to redirect users to untrusted domains
    return urljoin(request.host_url, target)