from flask import (
    Blueprint,
    render_template,
    flash,
    request,
    redirect,
    url_for,
    jsonify)

from flask_login import (
    login_required,
    login_user,
    current_user,
    logout_user)

from sabang.database.apartment_models import Apartment, Apt_Price, Apt_Item
from sabang.utils.room_helpers import delete_rooms, add_room, update_items, update_prices
from sabang.utils.response_handler import prep_resp

# var = Blueprint('name_of_bp')
apartments_api = Blueprint('apartments_api', __name__, template_folder='templates')

@apartments_api.route('/update_info/<int:id>', methods=['GET', 'POST'])
@login_required
def room_update_info(id):
    fdata = request.get_json()
    selected_apt = Apartment.query.get(id)
    selected_apt.name = fdata['data']['name'],
    selected_apt.description = fdata['data']['desc'],
    selected_apt.info = fdata['data']['info'],
    selected_apt.save()
    return prep_resp(msg="Apartment updated!")

@apartments_api.route('/update_prices/<int:id>', methods=['GET', 'POST'])
@login_required
def room_price_update(id):
    fdata = request.get_json()
    data = update_prices(fdata['data'], "apt")
    if data == "Success":
        return prep_resp(msg="Prices Updated", success=True)
    else:
        return prep_resp(msg="Error", success=False)


@apartments_api.route('/add_new_price', methods=['GET', 'POST'])
@login_required
def add_apt_price():
    fdata = request.get_json()
    current_apt = Apartment.query.get(fdata['id'])
    new_price_params = {
        "room_id": fdata['id'],
        "number_people": fdata['capacity'],
        "price": fdata['price'],
        "room_type": fdata['type'],
    }
    Apt_Price(apartment=current_apt, **new_price_params).save()
    return prep_resp(msg="Price Added!", success=True)

@apartments_api.route('/update_items/<int:id>', methods=['GET', 'POST'])
@login_required
def update_apartment_items(id):
    fdata = request.get_json()
    data = update_items(fdata['data'], "apt")
    if data == "Success":
        return prep_resp(msg="Items Updated", success=True)
    else:
        return prep_resp(msg="Error", success=False)

@apartments_api.route('/add_new_item/<int:id>', methods=['GET', 'POST'])
@login_required
def new_apartment_item(id):
    fdata = request.get_json()
    current_apt = Apartment.query.get(id)
    new_item_params = {
        "room_id": id,
        "item_name": fdata['item_name'],
        "item_description": fdata['item_desc'],
    }

    Apt_Item(apartment=current_apt, **new_item_params).save()
    return prep_resp(msg="Item Added!", success=True)


@apartments_api.route('/delete', methods=['GET', 'POST'])
@login_required
def delete_apartment():
    try:
        fdata = request.get_json()
        result = delete_rooms(fdata['data'], "apt")
        return prep_resp(msg="Success")
    except Exception as e:

        return prep_resp(msg="Error")

@apartments_api.route('/add', methods=['GET', 'POST'])
@login_required
def add_apt():
    try:
        fdata = request.get_json()
        result = add_room(fdata['data'], "apt")
        return prep_resp(msg="Success")
    except Exception as e:

        return prep_resp(msg="Error")