from flask import (
    Blueprint,
    render_template,
    flash,
    request,
    redirect,
    url_for,
    jsonify)

from flask_login import (
    login_required,
    login_user,
    current_user,
    logout_user)

from sabang.database.apartment_models import Apartment
from sabang.utils.room_helpers import parse_room_data, selected_room_data

apartments = Blueprint('apartments', __name__, template_folder='templates')

@apartments.route('/', methods=['GET', 'POST'])
@login_required
def apartments_index():
    apt_list = Apartment.query.all()
    data = parse_room_data(apt_list)
    return render_template('apartments/index.html', apartment_list=data)

@apartments.route('/apartment_detail/<int:id>', methods=['GET', 'POST'])
@login_required
def apartments_detail(id):
    apt = Apartment.query.get(id)
    apt_data = selected_room_data(apt)
    print 'CHECK THE IMG URL!!!'
    print apt_data
    return render_template('apartments/apt_detail.html', apartment=apt_data)

