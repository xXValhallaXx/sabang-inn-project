from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, PasswordField
from wtforms.validators import DataRequired, Length, Optional
from wtforms import validators

class UpdateForm(FlaskForm):
    first_name = StringField('First Name', [DataRequired(), Length(3, 254)])
    last_name = StringField('Last Name', [DataRequired(), Length(3, 128)])
    email = StringField('Email', [DataRequired(), Length(3, 128)])
    password = PasswordField('Password', [Optional(), Length(8, 128)])