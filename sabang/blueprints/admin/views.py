from flask import (
    Blueprint,
    render_template,
    flash,
    request,
    redirect,
    url_for)

from flask_login import (
    login_required,
    current_user)

from sabang.database.employee_models import Employee
from .forms import UpdateForm

# var = Blueprint('name_of_bp')
admin = Blueprint('admin', __name__, template_folder='templates')

@admin.route('/dashboard')
@login_required
def dashboard():
    return render_template('admin/dashboard.html')

@admin.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    # Will automatically populare form with user properties
    form = UpdateForm(obj=current_user)
    # Populate form with existing user data
    # form.first_name.data = current_user.first_name

    if form.validate_on_submit():
        print 'FORM UPDATING'
        new_password = request.form.get('password', '')
        current_user.email = request.form.get('email')
        current_user.first_name = request.form.get('first_name')
        current_user.last_name = request.form.get('last_name')

        if new_password:
            current_user.password = Employee.encrypt_password(new_password)
        
        current_user.save()
        flash('Your details are updated!', 'success')
        return redirect(url_for('admin.dashboard'))
    if form.errors:
        flash(form.errors, 'danger')
    return render_template('admin/profile.html', form=form)

  