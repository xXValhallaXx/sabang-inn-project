from flask import Blueprint

from flask_login import login_required
from sabang.utils.response_handler import prep_resp

# var = Blueprint('name_of_bp')
admin_api = Blueprint('admin_api', __name__, template_folder='templates')

@admin_api.route('/', methods=['GET'])
@login_required
def room_prices():
    return prep_resp(msg="This is admin API")
