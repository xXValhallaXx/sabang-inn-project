from sabang.app import create_celery_app, mail
from sabang.database.employee_models import Employee
from flask_mail import Message

celery = create_celery_app()

@celery.task()
def deliver_password_reset_email(user_id, reset_token):
    """
        Send reset password email to user
    """
    print 'HITTING THIS TASK'
    # Good practice to send user id rather than the user object for background tasks
    # We need to do a DB lookup - BUT
    # Celery would need to serialize and desearialze the entire object (waste of space saved in redis)
    # OR if user somehow remembers their PW, and they change email before task is executed
    # This means celery would send to old email
    emp = Employee.query.get(user_id)

    if emp is None:
        return

    # This works for now
    msg = Message('Password Reset!', recipients=[emp.email])
    msg.body = 'http://localhost:5000/auth/password_reset?reset_token=' + reset_token
    mail.send(msg)

    return None