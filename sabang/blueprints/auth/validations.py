from wtforms.validators import ValidationError

from sabang.database.employee_models import Employee

def ensure_identity_exists(form, field):
    """
        Ensure a user exits
    """
    emp = Employee.find_by_identity(field.data)
    
    if not emp:
        raise ValidationError('Account not found!')