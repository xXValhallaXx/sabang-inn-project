from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, PasswordField
from wtforms.validators import DataRequired, Length

from validations import ensure_identity_exists

class LoginForm(FlaskForm):
    # HiddenField - Stores the next URL to redirect user to page they tried to access
    next = HiddenField()
    identity = StringField('Email', [DataRequired(), Length(3, 254)])
    password = PasswordField('Password', [DataRequired(), Length(8, 128)])

class RegisterForm(FlaskForm):
    email = StringField('Email', [DataRequired(), Length(3, 254)])
    password = PasswordField('Password', [DataRequired(), Length(8, 128)])
    first_name = StringField('First Name', [DataRequired(), Length(3, 254)])
    last_name = StringField('Last Name', [DataRequired(), Length(3, 254)])

class BeginPasswordResetForm(FlaskForm):
    identity = StringField('Email', [DataRequired(), Length(3, 254), ensure_identity_exists])

class PasswordResetForm(FlaskForm):
    reset_token = HiddenField()
    password = PasswordField('Password', [DataRequired(), Length(8, 128)])