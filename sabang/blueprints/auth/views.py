from flask import (
    Blueprint,
    render_template,
    flash,
    request,
    redirect,
    url_for)

from flask_login import (
    login_required,
    login_user,
    current_user,
    logout_user)

from sabang.utils.safe_next_url import safe_next_url
from sabang.utils.decorators import anoymous_required
from sabang.database.employee_models import Employee
from forms import RegisterForm, LoginForm, BeginPasswordResetForm, PasswordResetForm

auth = Blueprint('auth',__name__, template_folder='templates')

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have logged out!', 'success')
    return redirect(url_for('auth.login'))

@auth.route('/')
@auth.route('/login', methods=['GET', 'POST'])
@anoymous_required()
def login():
    # Passing in the URL to the next url field
    form = LoginForm(next=request.args.get('next'))
    if form.validate_on_submit():
        # Search for employee
        u = Employee.find_by_identity(request.form.get('identity'))
        # If user found and is active
        if u and u.authenticated(password=request.form.get('password')):
            if login_user(u, remember=True) and u.is_active():
                next_url = request.form.get('next')
                if next_url:
                    # safe_next_url is 
                    return redirect(safe_next_url(next_url))
                
                return redirect(url_for('admin.dashboard'))
            else:
                flash('This account has been disabled.', 'danger')
        else:
            flash('Identity or password is incorrect.', 'danger')
    return render_template('auth/login.html', form=form)

@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        u = Employee()
        email = request.form.get('email')
        check_emp = Employee.find_by_identity(email)
        if check_emp is None:
            # Simple way to assign form values to an object
            # Fills user model with values from the form
            form.populate_obj(u)
            u.password = Employee.encrypt_password(request.form.get('password'))
            u.save()

            if login_user(u):
                flash('You are now registered!', 'success')
                return redirect(url_for('admin.dashboard'))
        else:
            flash('User already in Database!', 'danger')

    return render_template('auth/register.html', form=form)

@auth.route('/begin_password_reset', methods=['GET','POST'])
@anoymous_required()
def begin_password_reset():
    form = BeginPasswordResetForm()

    if form.validate_on_submit():
        emp = Employee.initialize_password_reset(request.form.get('identity'))

        flash('An email has been sent to {0}.'.format(emp.email), 'success')
        return redirect(url_for('auth.login'))

    return render_template('auth/begin_password_reset.html', form=form)

@auth.route('/password_reset', methods=['GET', 'POST'])
@anoymous_required()
def password_reset():
    # Calling the password reset form while passing in the requesttoken from URL
    form = PasswordResetForm(reset_token=request.args.get('request_token'))

    if form.validate_on_submit():
        print 'THE TOKEN WE GOT'
        print request.args.get('request_token')
        emp = Employee.deserialze_token(request.form.get('reset_token'))

        if emp is None:
            flash('Token expired or tampered with!', 'danger')
            return redirect(url_for('auth.begin_password_reset'))

        form.populate_obj(emp)
        emp.password = Employee.encrypt_password(request.form.get('password'))
        emp.save()

        if login_user(emp):
            flash('Your password has been updated!', 'success')
            return redirect(url_for('admin.profile'))
    
    return render_template('auth/password_reset.html', form=form)