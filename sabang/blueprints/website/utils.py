def parse_room_data(rooms):
    parsed_data = []
    current_room = {}
    current_room_items = []
    current_room_prices = []
    current_room_images = []
    for room in rooms:
        room_info = {
            'id': room.id,
            'description': room.description,
            'name': room.name,
            'info': room.info,
        }
        current_room['room_info'] = room_info

        room_prices = room.prices.all()
        for price in room_prices:
            price_data = {
                "price": price.price
            }
            current_room_prices.append(price_data)
            current_room['prices'] = current_room_prices
        current_room_prices = []

        room_items = room.items.all()
        for item in room_items:
            item_data = {
                "item": item.item_name
            }
            current_room_items.append(item_data)
            current_room['items'] = current_room_items
        current_room_items = []

        room_images = room.images.all()
        for image in room_images:
            image_data = {
                "img_url": image.img_url,
                "avatar": image.avatar,
                "img_id": image.img_id
            }
            current_room_images.append(image_data)
            current_room['images'] = current_room_images
        current_room_images = []

        parsed_data.append(current_room)
        current_room = {}
    return parsed_data