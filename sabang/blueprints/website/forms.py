from flask_wtf import Form
from wtforms import TextAreaField
from wtforms_components import EmailField
from wtforms.validators import DataRequired, Length
from wtforms import validators

class ContactForm(Form):
    email = EmailField('Email: ',
                        [validators.Length(min=5, message=(u'Please enter a valid email address...'))])
    message = TextAreaField('Message: ',
                            [validators.Length(min=5, message=(u'Please enter a longer message so we can help you better...'))])