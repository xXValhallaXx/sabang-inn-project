from flask import (
    Blueprint,
    render_template,
    flash,
    redirect,
    request,
    url_for
)

from sabang.database.room_models import Room
from sabang.database.apartment_models import Apartment
from sabang.blueprints.website.forms import ContactForm
from sabang.blueprints.website.utils import parse_room_data

website = Blueprint('website', __name__, template_folder='templates')


@website.route('/', methods=['GET'])
def index():
    return render_template('website/index.html')

@website.route('/about_us', methods=['GET'])
def about_us():
    return render_template('website/about_us.html')

@website.route('/rooms', methods=['GET'])
def rooms():
    room_data = parse_room_data(Room.query.all())
    return render_template('website/rooms.html', rooms=room_data)

@website.route('/restaurant', methods=['GET'])
def restaurant():
    return render_template('website/restaurant.html')

@website.route('/apartments', methods=['GET'])
def apartments():
    aparmtent_data = parse_room_data(Apartment.query.all())
    print(aparmtent_data)
    return render_template('website/apartments.html', apartments=aparmtent_data)

@website.route('/diving', methods=['GET'])
def diving():
    return render_template('website/diving.html')

@website.route('/packages', methods=['GET'])
def packages():
    return render_template('website/packages.html')

@website.route('/contact', methods=['GET', 'POST'])
def contact_us():
    form = ContactForm()

    # Differentiaes GET/POST requests
    # validate_on_submit() is from WTForms
    # If form is submit, and no errors then everything below is executed
    # This is a "POST"
    if form.validate_on_submit():
        from sabang.blueprints.website.tasks import send_contact_form

        # .delay tells Celery we want task to run in background
        send_contact_form.delay(
            request.form.get('email'),
            request.form.get('message'))
        
        # Allows us to display Flask Flash message
        # Message is seen after being redirected
        # Send 'success' to style with Bootstrap
        flash("Thanks for the message, we will get back to you shortly!", 'success')
        # Redirect back to contact page - This will allow for form to be cleared
        return redirect(url_for('website.contact_us'))
    # GET requests
    # We send the form arguement to the jinja2 template
    return render_template('website/contact_us.html', form=form)
