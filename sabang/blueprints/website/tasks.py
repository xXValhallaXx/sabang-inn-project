from sabang.app import create_celery_app, mail
from flask_mail import Message
from flask import render_template

# Create instance of create_celery_app
celery = create_celery_app()

@celery.task()
def send_contact_form(email, message):

    msg = Message('General Inquiry', recipients=['info@sabanginn.com'])
    msg.html = render_template('website/contact_email.html', customer_email=email, customer_message=message)
    mail.send(msg)

    return None