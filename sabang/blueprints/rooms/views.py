from flask import (
    Blueprint,
    render_template)

from flask_login import login_required

from sabang.database.room_models import Room
from sabang.utils.room_helpers import parse_room_data, selected_room_data

rooms = Blueprint('rooms', __name__, template_folder='templates')

@rooms.route('/', methods=['GET', 'POST'])
@login_required
def rooms_index():
    room_list = Room.query.all()
    data = parse_room_data(room_list)
    return render_template('rooms/index.html', room_list=data)


@rooms.route('/room_detail/<int:id>', methods=['GET', 'POST'])
@login_required
def room_detail(id):
    room = Room.query.get(id)
    room_data = selected_room_data(room)
    print "ROOOOMZZZZ"
    print room_data
    return render_template('rooms/room_detail.html', room=room_data)