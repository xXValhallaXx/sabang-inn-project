from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, PasswordField
from wtforms.validators import DataRequired, Length, Optional
from wtforms import validators

class UpdateForm(FlaskForm):
    first_name = StringField('First Name', [DataRequired(), Length(3, 254)])
    last_name = StringField('Last Name', [DataRequired(), Length(3, 128)])
    email = StringField('Email', [DataRequired(), Length(3, 128)])
    password = PasswordField('Password', [Optional(), Length(8, 128)])

class UpdateRoomInfoForm(FlaskForm):
    room_name = StringField('Room Name', [DataRequired(), Length(3, 254)])
    room_desc = StringField('Room Description', [DataRequired(), Length(3, 128)])
    room_info = StringField('Room Info', [DataRequired(), Length(3, 128)])

class AddItemForm(FlaskForm):
    item_name = StringField('New Item',
                            [validators.Length(min=1, message=(u'Field can not be left blank!'))])


class UpdateItemForm(FlaskForm):
    update_item = StringField('Update Item', [DataRequired(), Length(3, 100)])

class AddPriceForm(FlaskForm):
    room_type = StringField('Room Type', [DataRequired(), Length(3, 100)])
    room_capacity = StringField('Room Capacity', [DataRequired(), Length(3, 100)])
    room_price = StringField('Room Price', [DataRequired(), Length(3, 100)])