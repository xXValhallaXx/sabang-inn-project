import os
from random import *
from flask import (
    Blueprint,
    render_template,
    flash,
    request,
    redirect,
    url_for,
    jsonify)

from flask_login import (
    login_required,
    login_user,
    current_user,
    logout_user)

from sabang.database.room_models import Room, Item, Price, Image
from sabang.utils.response_handler import prep_resp
from sabang.utils.room_helpers import delete_rooms, add_room, update_items, update_prices
from sabang.extensions import db

rooms_api = Blueprint('rooms_api', __name__, template_folder='templates')

@rooms_api.route('/delete_room', methods=['POST'])
@login_required
def delete_room_api():
    try:
        fdata = request.get_json()
        result = delete_rooms(fdata['data'], "room")
        return prep_resp(msg="Success")
    except Exception as e:
        return prep_resp(msg="Error")

@rooms_api.route('/add_room', methods=['POST'])
@login_required
def add_room_api():
    try:
        fdata = request.get_json()
        result = add_room(fdata['data'], "room")

        return prep_resp(msg="Success")
    except Exception as e:
        return prep_resp(msg="Error")


@rooms_api.route('/add_new_price', methods=['POST'])
@login_required
def add_new_price():
    fdata = request.get_json()
    current_room = Room.query.get(fdata['id'])
    new_price_params = {
        "room_id": fdata['id'],
        "price": fdata['price'],
    }
    
    Price(room=current_room, **new_price_params).save()
    return prep_resp(msg="Price Added!", success=True)

@rooms_api.route('/update_info/<int:id>', methods=['POST'])
@login_required
def update_room_info(id):
    fdata = request.get_json()
    selected_room = Room.query.get(id)
    selected_room.name = fdata['data']['name'],
    selected_room.description = fdata['data']['desc'],
    selected_room.info = fdata['data']['info'],
    selected_room.save()
    return prep_resp(msg="Room updated!")

@rooms_api.route('/add_new_item/<int:id>', methods=['POST'])
@login_required
def add_new_item(id):
    fdata = request.get_json()
    current_room = Room.query.get(id)
    new_item_params = {
        "room_id": id,
        "item_name": fdata['data']['item_name'],
        "item_description": "Default",
    }
    Item(room=current_room, **new_item_params).save()
    return prep_resp(msg="Price Added!", success=True)


@rooms_api.route('/update_items/<int:id>', methods=['POST'])
@login_required
def update_apt_items(id):
    fdata = request.get_json()
    data = update_items(fdata['data'], "room")
    if data == "Success":
        return prep_resp(msg="Items Updated", success=True)
    else:
        return prep_resp(msg="Error", success=False)


@rooms_api.route('/update_prices/<int:id>', methods=['POST'])
@login_required
def update_apt_prices(id):
    fdata = request.get_json()
    data = update_prices(fdata['data'], "room")
    if data == "Success":
        return prep_resp(msg="Prices Updated", success=True)
    else:
        return prep_resp(msg="Error", success=False)


@rooms_api.route('/img_upload/<int:id>', methods=['POST', 'GET'])
@login_required
def add_new_room(id):
  count = Image.query.filter(Image.room_id == id).count()

  if count >= 6:
    return prep_resp(msg="Remove other images before adding more thans", success=False)

  f = request.files['file']
  filePath = 'sabang/static/images/room_images/' + str(id)
  fullPath = 'sabang/static/images/room_images/' + str(id) + '/' + f.filename
  test = '/static/images/room_images/' + str(id) + '/' + f.filename
  if not os.path.exists(filePath):
    try:
      os.makedirs(filePath)
    except Exception as e:
      print "ERROR"



  f.save(fullPath)
  current_room = Room.query.get(id)
  new_item_params = {
      "room_id": id,
      "img_id": random(),
      "img_url": test,
  }
  Image(room=current_room, **new_item_params).save()
  return prep_resp(msg="Uploaded!", success=True)


@rooms_api.route('/image/remove/<int:id>', methods=["POST", "GET"])
@login_required
def remove_room_image(id):
  fdata = request.get_json()
  for x in fdata['data']:
    result = Image.query.get(x['imgID'])
    db.session.delete(result)
  db.session.commit()
  return prep_resp(msg="Done", success=True)