from flask import Flask
from celery import Celery
from itsdangerous import URLSafeTimedSerializer
from werkzeug.contrib.fixers import ProxyFix

from sabang.database.employee_models import Employee

# Register Blueprints
from sabang.blueprints.admin import admin as admin_bp
from sabang.blueprints.admin.api import admin_api
from sabang.blueprints.website import website as website_bp
from sabang.blueprints.auth import auth as auth_bp
from sabang.blueprints.apartments import apartments as apartment_bp
from sabang.blueprints.apartments.api import apartments_api
from sabang.blueprints.rooms import rooms as room_bp
from sabang.blueprints.rooms.api import rooms_api


# Import our extensions
from sabang.extensions import (
    debug_toolbar,
    csrf,
    mail,
    db,
    login_manager
)

CELERY_TASK_LIST = [
    'sabang.blueprints.website.tasks',
    'sabang.blueprints.auth.tasks'
]

def create_celery_app(app=None):
    """
    Create an instance of Celery
    """
    app = app or create_app()

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],
                    include=CELERY_TASK_LIST)
    celery.conf.update(app.config)
    TaskBase = celery.Task

    # Setup  a context for each task
    # If we plan to access DB with a task - context needs be set
    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery

def create_app():
    """
        Flask app factory pattern
        - Everything encapsulated in a single function
    """

    # instance_relative_config: Informs flask to look for 'instance' module
    # - Searches for the module in same directory as the sabang module
    app = Flask(__name__, instance_relative_config=True)

    # Look for config file in config module and get settings.py
    app.config.from_object('config.settings')
    # Searches for settings file in instance module
    # - silent flag is so application wont fail if not found
    app.config.from_pyfile('settings.py', silent=True)
    # Instance settings file will override config/settings because order declared

    # Call settings variables by: app.config['VAR_NAME]
    
    # Flask automatically sets log level to DEBUG

    app.logger.setLevel(app.config['LOG_LEVEL'])

    app.register_blueprint(website_bp)
    app.register_blueprint(auth_bp, url_prefix='/auth')
    app.register_blueprint(admin_bp, url_prefix='/admin')
    app.register_blueprint(admin_api, url_prefix='/admin/api/v1')
    app.register_blueprint(apartment_bp, url_prefix='/admin/apartment')
    app.register_blueprint(apartments_api, url_prefix='/admin/apartment/api/v1')
    app.register_blueprint(room_bp, url_prefix='/admin/room')
    app.register_blueprint(rooms_api, url_prefix='/admin/room/api/v1')

    # Call the extensions function
    extensions(app)
    authentication(app, Employee)
    return app

def extensions(app):
    """
    Extensions function takes Flask App instance
    - Initializes each extension into the app
    """
    debug_toolbar.init_app(app)
    mail.init_app(app)
    csrf.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)

    return None

def authentication(app, emp_model):

    login_manager.login_view = 'auth.login'
    login_manager.login_message = "You must be logged in for this!..."
    
    @login_manager.user_loader
    def load_user(uid):
        return emp_model.query.get(uid)

def middleware(app):
    # Swap request.remote_addr with the real IP address even if behind a proxy.
    app.wsgi_app = ProxyFix(app.wsgi_app)

    return None