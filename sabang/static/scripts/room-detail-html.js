function renderRoomPrices(roomPrices){
    let i = 0;
    let roomPriceRow = "";
    let roomPriceRows = "";
    console.log('THIS IS THE ROOMPRICES: ', roomPrices)
    for(i; i < roomPrices.length; i++){
        roomPriceRow = `
            <input value="${roomPrices[i].price}"  id="room-price${i}" />
            <input value="${roomPrices[i].capacity}" id="room-capacity${i}" />
            <input value="${roomPrices[i].room_type}" id="room-type${i}" />
        `;
        roomPriceRows = roomPriceRows +  "" + roomPriceRow + "";
    }
    return `
        <div>
            ${roomPriceRows}
            <button id="room-price-update-btn">Update Prices</button>
        </div>
    `;
    
};