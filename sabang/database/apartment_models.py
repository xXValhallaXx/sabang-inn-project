# Models which inherit from the DB model automatically makes it work with SQLAlchemy
from sabang.extensions import db
from sabang.utils.sql_alchemy_utils import ResourceMixin

class Apartment(ResourceMixin, db.Model):

    __tablename__ = 'apartments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), default="Room Name", nullable=False)
    description = db.Column(db.String(250), default="Description", nullable=False)
    info = db.Column(db.String(250), nullable=True)
    img_url = db.Column(db.String(256), nullable=True)
    prices = db.relationship('Apt_Price', backref='apartment', lazy='dynamic', cascade="all, delete-orphan")
    images = db.relationship('Apt_Image', backref='apartment', lazy='dynamic', cascade="all, delete-orphan")
    items = db.relationship('Apt_Item', backref='apartment', lazy='dynamic', cascade="all, delete-orphan")

    def __init__(self, **kwargs):
        super(Apartment, self).__init__(**kwargs)

class Apt_Price(ResourceMixin, db.Model):
    
    __tablename__ = "apt_prices"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey('apartments.id'))
    room_type = db.Column(db.String(60), default="Aircon/Fan", nullable=False)
    number_people = db.Column(db.Integer, default=0, nullable=False)
    price = db.Column(db.Integer, nullable=False, default=0)

    def __init__(self, **kwargs):
        super(Apt_Price, self).__init__(**kwargs)

class Apt_Item(ResourceMixin, db.Model):
    
    __tablename__ = "apt_items"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey('apartments.id'))
    item_name = db.Column(db.String(60), default="Item Name", nullable=False)
    item_description = db.Column(db.String(60), nullable=True)

    def __init__(self, **kwargs):
        super(Apt_Item, self).__init__(**kwargs)

class Apt_Image(ResourceMixin, db.Model):
    
    __tablename__ = "apt_images"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey('apartments.id'))
    avatar = db.Column(db.Boolean, default=False, nullable=False)
    img_id = db.Column(db.Integer, nullable=False)
    img_url = db.Column(db.String(128), default="http://www.example.com", nullable=False)

    def __init__(self, **kwargs):
        super(Apt_Image, self).__init__(**kwargs)