from flask import current_app
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer
from flask_login import UserMixin

# Models which inherit from the DB model automatically makes it work with SQLAlchemy
from sabang.extensions import db
from sabang.utils.sql_alchemy_utils import ResourceMixin

# ResourceMixin is a custom class to add added functionality
# Models which inherit from db.Model automatically makes it work with SQLAlchemy
class Employee(UserMixin, ResourceMixin, db.Model):

    __tablename__ = 'employees'
    id = db.Column(db.Integer, primary_key=True)
    # Need this is_active to satisfy flask-login
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')
    email = db.Column(db.String(255), unique=True, index=True, nullable=False, server_default='')
    password =  db.Column(db.String(128), nullable=False, server_default='')
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128), nullable=False)


    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy constructor
        super(Employee, self).__init__(**kwargs)
        self.password = Employee.encrypt_password(kwargs.get('password', ''))

    @classmethod
    def find_by_identity(cls, email):

        #current_app.logger.debug('{0} Has tried to login:'.format(email))
        
        # Allows us to search for employee via their email
        return Employee.query.filter(
          (Employee.email == email)).first()

    @classmethod
    def encrypt_password(cls, plaintext_password):
 
        # generate_password_hash is built into the werzkerg security module
        # uses PBKDF2 to encrypt the password
        if plaintext_password:
            return generate_password_hash(plaintext_password)

        return None

    @classmethod
    def initialize_password_reset(cls, identity):
        """
            Generate a toke to reset PW
        """
        emp = Employee.find_by_identity(identity)
        reset_token = emp.serialize_token()

        # Prevents circular imports
        from sabang.blueprints.auth.tasks import (
            deliver_password_reset_email)
        # Kicks off the celery task
        deliver_password_reset_email.delay(emp.id, reset_token)

        return emp
    
    def serialize_token(self, expiration=3600):
        """
            Sign and create token for user
            Token lasts for 1 hr
        """
        # Uses apps secret key value to sign token
        private_key = current_app.config['SECRET_KEY']
        # Uses isitdangerous to create timed token
        serializer = TimedJSONWebSignatureSerializer(private_key, expiration)
        # Create the user email in utf-8 JSON format
        return serializer.dumps({'user_email': self.email}).decode('utf-8')

    @classmethod
    def deserialze_token(cls, token):
        """
            Obtain a user from de-searializing the token
        """
        private_key = TimedJSONWebSignatureSerializer(
            current_app.config['SECRET_KEY'])
        
        try:
            decoded_payload = private_key.loads(token)
            print decoded_payload
            return Employee.find_by_identity(decoded_payload.get('user_email'))
        except Exception:
            return None

    def is_active(self):
        return self.active

    def authenticated(self, with_password=True, password=''):
        # Takes users unencrypted password then compares to the users encrypted user
        if with_password:
            return check_password_hash(self.password, password)
        
        # Make false in prodction
        return True