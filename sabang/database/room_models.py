# Models which inherit from the DB model automatically makes it work with SQLAlchemy
from sabang.extensions import db
from sabang.utils.sql_alchemy_utils import ResourceMixin

class Room(ResourceMixin, db.Model):

    __tablename__ = 'rooms'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), default="Room Name", nullable=False)
    description = db.Column(db.String(250), default="Description", nullable=False)
    info = db.Column(db.String(250), nullable=True)
    prices = db.relationship('Price', backref='room', lazy='dynamic', cascade="all, delete-orphan")
    images = db.relationship('Image', backref='room', lazy='dynamic', cascade="all, delete-orphan")
    items = db.relationship('Item', backref='room', lazy='dynamic', cascade="all, delete-orphan")

    def __init__(self, **kwargs):
        super(Room, self).__init__(**kwargs)

class Price(ResourceMixin, db.Model):
    
    __tablename__ = "prices"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'))
    price = db.Column(db.Integer, nullable=False, default=0)

    def __init__(self, **kwargs):
        super(Price, self).__init__(**kwargs)

class Item(ResourceMixin, db.Model):
    
    __tablename__ = "items"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'))
    item_name = db.Column(db.String(60), default="Item Name", nullable=False)
    item_description = db.Column(db.String(60), nullable=True)

    def __init__(self, **kwargs):
        super(Item, self).__init__(**kwargs)

class Image(ResourceMixin, db.Model):
    
    __tablename__ = "images"

    id = db.Column(db.Integer, primary_key=True)
    room_id = db.Column(db.Integer, db.ForeignKey('rooms.id'))
    img_id = db.Column(db.Integer, nullable=False)
    avatar = db.Column(db.Boolean, default=False, nullable=False)
    img_url = db.Column(db.String(128), default="http://www.example.com", nullable=False)

    def __init__(self, **kwargs):
        super(Image, self).__init__(**kwargs)