from sabang.app import create_app

from flask_script import Manager
from flask_migrate import MigrateCommand

from sabang.extensions import db
from sabang.database.employee_models import Employee
from sabang.database.room_models import Room, Price, Item, Image
from sabang.database.apartment_models import Apartment, Apt_Price, Apt_Item, Apt_Image
from instance import settings

app = create_app()
db.app = app

manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.command
def init_db():
    """
        Initialize the DB
    """
    db.drop_all()
    db.create_all()


@manager.command
def reset_db():
    """
        Resets the DB and Adds User Roles
    """
    db.drop_all()
    db.create_all()
    create_admin()
    print 'DB Reset - Roles Created!'
    seed_rooms(app)
    print 'Rooms Created'
    seed_apartments(app)
    print 'Apartments made'


@manager.command
def create_admin():
    admin = Employee(email=settings.SEED_ADMIN_EMAIL,
                    password=settings.SEED_ADMIN_PASSWORD,
                    first_name=settings.SEED_FIRST_NAME,
                    last_name=settings.SEED_LAST_NAME)
    db.session.add(admin)
    db.session.commit()
    print "Admin Added!"


def seed_rooms(app):
   
    standard_room = {
        'name': "Standard",
        'description': 'We provide you everything you need for you to ensure that your stay is at comfortable as possible, with an extremely competitive price.',
        "info": 'Our standard rooms provide you a luxurious stay, with the perfect location right next to the bay.',
    }

    standard_room_price = {
        'price': 1600,
    }

    sea_view_item = {
        "item_name": "Sea View",
    }

    partial_sea_view_item = {
        "item_name": "Partial Sea View",
    }

    refrigerator_item = {
        "item_name": "Refrigerator",
    }

    balcony_item = {
        "item_name": "Private Balcony",
    }

    veranda_item = {
        "item_name": "Veranda",
    }

    tv_21_item = {
        "item_name": "21' Cable TV",
    }

    tv_29_item = {
        "item_name": "29' Cable TV",
    }

    tv_32_item = {
        "item_name": "21' Cable LCD TV",
    }

    shower_item = {
        "item_name": "Hot Shower",
    }

    fully_furnished_item = {
        "item_name": "Fully Furnished",
    }

    spring_mattress_item = {
        "item_name": "Sprint Mattress",
    }

    kitchen_item = {
        "item_name": "Kitchen",
    }

    deposit_box_item = {
        "item_name": "Safety deposit box"
    }

    standard_room_img_1 = {
        "img_url": "/static/images/room_images/1/standard_1.jpg",
        "img_id": 1,
        "avatar": True
    }

    standard_room_img_2 = {
        "img_url": "/static/images/room_images/1/standard_2.jpg",
        "img_id": 2,
    }

    standard_room_img_3 = {
        "img_url": "/static/images/room_images/1/standard_3.jpg",
        "img_id": 3,
    }

    standard_room_img_4 = {
        "img_url": "/static/images/room_images/1/standard_4.png",
        "img_id": 4,
    }

    # The ** allows us to use the dict and change into keyword arguments
    seed_standard_room = Room(**standard_room)
    Price(room=seed_standard_room, **standard_room_price).save()
    Item(room=seed_standard_room, **sea_view_item).save()
    Item(room=seed_standard_room, **balcony_item).save()
    Item(room=seed_standard_room, **tv_21_item).save()
    Item(room=seed_standard_room, **shower_item).save()
    Item(room=seed_standard_room, **deposit_box_item).save()
    Item(room=seed_standard_room, **refrigerator_item).save()
    Image(room=seed_standard_room, **standard_room_img_1).save()
    Image(room=seed_standard_room, **standard_room_img_2).save()
    Image(room=seed_standard_room, **standard_room_img_3).save()
    Image(room=seed_standard_room, **standard_room_img_4).save()
    
    deluxe_room = {
        'name': "Deluxe",
        'description': 'These rooms include your own private veranda, allowing you to enjoy the sun quietly on your vacation.',
        "info": 'Our deluxe rooms offer a slightly larger living area, with a better view of the bay.',
    }

    deluxe_room_price = {
        'price': 1900,
    }

    deluxe_room_img_1 = {
        "img_url": "/static/images/room_images/2/deluxe_room_1.jpg",
        "img_id": 1,
        "avatar": True
    }

    deluxe_room_img_2 = {
        "img_url": "/static/images/room_images/2/deluxe_room_2.jpg",
        "img_id": 2,
    }

    deluxe_room_img_3 = {
        "img_url": "/static/images/room_images/2/deluxe_room_3.jpg",
        "img_id": 3,
    }

    deluxe_room_img_4 = {
        "img_url": "/static/images/room_images/2/deluxe_room_4.png",
        "img_id": 4,
    }

    seed_deluxe_room = Room(**deluxe_room)
    Price(room=seed_deluxe_room, **deluxe_room_price).save()
    Item(room=seed_deluxe_room, **sea_view_item).save()
    Item(room=seed_deluxe_room, **veranda_item).save()
    Item(room=seed_deluxe_room, **tv_32_item).save()
    Item(room=seed_deluxe_room, **shower_item).save()
    Item(room=seed_deluxe_room, **deposit_box_item).save()
    Item(room=seed_deluxe_room, **refrigerator_item).save()
    Image(room=seed_deluxe_room, **deluxe_room_img_1).save()
    Image(room=seed_deluxe_room, **deluxe_room_img_2).save()
    Image(room=seed_deluxe_room, **deluxe_room_img_3).save()
    Image(room=seed_deluxe_room, **deluxe_room_img_4).save()

    # ADDED JUST FOR THE SAKE OF DB POPULATED
    superior_room = {
        'name': "VIP Room",
        'description': '',
        "info": 'Our Superiour rooms, provide the ultimate comfort for our guests.',
    }

    superior_room_price = {
        'price': 2900,
    }

    superior_room_img_1 = {
        "img_url": "/static/images/room_images/3/superior-1.jpg",
        "img_id": 1,
        "avatar": True
    }

    superior_room_img_2 = {
        "img_url": "/static/images/room_images/3/superior-2.jpg",
        "img_id": 2,
    }

    superior_room_img_3 = {
        "img_url": "/static/images/room_images/3/superior-3.jpg",
        "img_id": 3,
    }

    superior_room_img_4 = {
        "img_url": "/static/images/room_images/3/superior-4.jpg",
        "img_id": 4,
    }


    seed_superior_room = Room(**superior_room)
    Price(room=seed_superior_room, **superior_room_price).save()
    Item(room=seed_superior_room, **fully_furnished_item).save()
    Item(room=seed_superior_room, **kitchen_item).save()
    Item(room=seed_superior_room, **balcony_item).save()
    Item(room=seed_superior_room, **sea_view_item).save()
    Item(room=seed_superior_room, **spring_mattress_item).save()
    Item(room=seed_superior_room, **tv_29_item).save()
    Item(room=seed_superior_room, **shower_item).save()
    Item(room=seed_superior_room, **deposit_box_item).save()
    Item(room=seed_superior_room, **refrigerator_item).save()
    Image(room=seed_superior_room, **superior_room_img_1).save()
    Image(room=seed_superior_room, **superior_room_img_2).save()
    Image(room=seed_superior_room, **superior_room_img_3).save()
    Image(room=seed_superior_room, **superior_room_img_4).save()



def seed_apartments(app):

    apartment_one = {
        'name': "Basic Studio",
        'info': "These are our most affordable long stay units that we offer, the perfect getaway from city life, at an attractive price.",
        'description': 'These units provide everything needed to ensure that your stay as pleasant as possible.',
    }

    apartment_one_price = {
        'price': 14000,
    }
    sea_view_item = {
        "item_name": "Sea View",
    }
    deposit_box_item = {
        "item_name": "Deposit Box",
    }
    refrigerator_item = {
        "item_name": "Refrigerator",
    }

    tv_item = {
        "item_name": "Large Cable TV",
    }

    shower_item = {
        "item_name": "Hot Shower",
    }

    fully_furnished_item = {
        "item_name": "Fully Furnished",
    }

    kitchen_item = {
        "item_name": "Kitchen",
    }

    internet_item = {
        "item_name": "WiFi Internet",
    }


    room_img_1 = {
        "img_url": "/static/images/apt_images/1/Basic_Apt_Studio_1.jpg",
        "img_id": 1,
        "avatar": True
    }

    room_img_2 = {
        "img_url": "/static/images/apt_images/1/Basic_Apt_Studio_2.jpg",
        "img_id": 2,
    }

    room_img_3 = {
        "img_url": "/static/images/apt_images/1/Basic_Apt_Studio_3.jpg",
        "img_id": 3,
    }

    new_apartment_one = Apartment(**apartment_one)
    Apt_Price(apartment=new_apartment_one, **apartment_one_price).save()
    Apt_Item(apartment=new_apartment_one, **sea_view_item).save()
    Apt_Item(apartment=new_apartment_one, **deposit_box_item).save()
    Apt_Item(apartment=new_apartment_one, **refrigerator_item).save()
    Apt_Item(apartment=new_apartment_one, **tv_item).save()
    Apt_Item(apartment=new_apartment_one, **shower_item).save()
    Apt_Item(apartment=new_apartment_one, **fully_furnished_item).save()
    Apt_Item(apartment=new_apartment_one, **kitchen_item).save()
    Apt_Image(apartment=new_apartment_one, **room_img_1).save()
    Apt_Image(apartment=new_apartment_one, **room_img_2).save()
    Apt_Image(apartment=new_apartment_one, **room_img_3).save()

    apartment_two = {
        'name': "Modern Studio",
        "info": "These are our largest units that we have to offer, providing ample room for those long vacations away from home.",
        'description': 'Select units have thier own minature pool, so you can cool off in the heat, without any disturbance ensuring you have a truly stress free vacation.',
    }

    apartment_two_price = {
        'price': 16000,
    }

    modern_apt_img_1 = {
        "img_url": "/static/images/apt_images/2/Modern_Studio_1.jpg",
        "img_id": 1,
        "avatar": True
    }

    modern_apt_img_2 = {
        "img_url": "/static/images/apt_images/2/Modern_Studio_2.jpg",
        "img_id": 2,
    }

    modern_apt_img_3 = {
        "img_url": "/static/images/apt_images/2/Modern_Studio_3.jpg",
        "img_id": 3,
    }

    modern_apt_img_4 = {
        "img_url": "/static/images/apt_images/2/Modern_Studio_4.jpg",
        "img_id": 4,
    }

    modern_apt_img_5 = {
        "img_url": "/static/images/apt_images/2/Modern_Studio_5.jpg",
        "img_id": 5,
    }


    new_apartment_two = Apartment(**apartment_two)
    Apt_Price(apartment=new_apartment_two, **apartment_two_price).save()
    Apt_Item(apartment=new_apartment_two, **sea_view_item).save()
    Apt_Item(apartment=new_apartment_two, **deposit_box_item).save()
    Apt_Item(apartment=new_apartment_two, **refrigerator_item).save()
    Apt_Item(apartment=new_apartment_two, **tv_item).save()
    Apt_Item(apartment=new_apartment_two, **shower_item).save()
    Apt_Item(apartment=new_apartment_two, **fully_furnished_item).save()
    Apt_Item(apartment=new_apartment_two, **kitchen_item).save()
    Apt_Image(apartment=new_apartment_two, **modern_apt_img_1).save()
    Apt_Image(apartment=new_apartment_two, **modern_apt_img_2).save()
    Apt_Image(apartment=new_apartment_two, **modern_apt_img_3).save()
    Apt_Image(apartment=new_apartment_two, **modern_apt_img_4).save()
    Apt_Image(apartment=new_apartment_two, **modern_apt_img_5).save()

if __name__ == "__main__":
    manager.run()
