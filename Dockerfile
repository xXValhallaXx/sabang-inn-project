FROM python:2.7-slim
MAINTAINER Renate Gouveia

ENV INSTALL_PATH /sabang
RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD gunicorn -b 0.0.0.0:8000 --access-logfile - "sabang.app:create_app()"

#CMD gunicorn -c "python:config.gunicorn.py" "sabang.app:create_app()"